<?php

require('configs/include.php');

class c_listarjugador extends super_controller {
    
    public function consultar(){
        
        function existeEquipo($objeto, $codi)
        {
            $objeto->orm->connect();

            $cod['equipo']['codigo'] = $codi;
            $options['equipo']['lvl2']="count_by_cod";
            $objeto->orm->read_data(array("equipo"),$options,$cod);
                                    
            $datos = $objeto->orm->data;
            $resultado = $datos['equipo'][0];
            $contador = $resultado->contador;

            $objeto->orm->close();

            if($contador ==  1)
            {
                return 1;
            } else {
                return 0;
            }
        }
        
        $team = new equipo($this->post);
        
        if(is_empty($team->get('codigo'))) {
            throw_exception("Debe ingresar un codigo para el Equipo");
        } else if(existeEquipo($this, $team->get('codigo')) == 0){
            $eq = $team->get('codigo');
            throw_exception("El Equipo con código = $eq no existe");
        } else {
            $eq = $team->get('codigo');
            $cod['jugador']['equipo'] = $eq;
            $options['jugador']['lvl2']="by_eq";
            
            $this->orm->connect();
            @$this->orm->read_data(array("jugador"),$options);
            print_r2($this);
            @$jugadores = $this->orm->get_objects("jugador");
            @$this->orm->close();

            $this->engine->assign('jugadores',$jugadores);
            $this->engine->assign('codeq',$eq);
        }        
    }
    
    public function display()
    {	
        $team = new equipo($this->post);
        $this->engine->assign('title','Listar Jugadores');
        $this->engine->display('header.tpl');        
        $this->engine->display('buscarcod.tpl');       
        $this->engine->display('footer.tpl');
    }

    public function run()
    {
        try {
            if (isset($this->get->option))
            {
                $this->{$this->get->option}();
            }
        }
        catch (Exception $e) 
        {
            $this->error=1; $this->msg_warning=$e->getMessage();
            $this->engine->assign('type_warning',$this->type_warning);
            $this->engine->assign('msg_warning',$this->msg_warning);
            $this->temp_aux = 'message.tpl';
        }    
        $this->display();
    }
}

$call = new c_listarjugador();
$call->run();

?>