<?php
    class jugador extends object_standard {
        
        //attribute variables
        protected $cedula;
        protected $nombre;
        protected $posicion;
        protected $equipo;
        
        //components
        var $components = array();

        //auxiliars for primary key and for files
        var $auxiliars = array();

        //data about the attributes
        public function metadata() {
            return array("cedula" => array(), "nombre" => array(), "posicion" => array(),
            "equipo" => array("foreign_name" => "e_j", "foreign" => "equipo", "foreign_attribute" => "codigo"));
        }

        public function primary_key() {
            return array("cedula");
        }

        public function relational_keys($class, $rel_name) {
            switch($class) 
            {
                case "equipo":
                    switch($rel_name) {
                        case "e_j":
                            return array("equipo");
                            break;
                    }
                    break;
                default:
                    break;
            }
	}    
    }
?>
