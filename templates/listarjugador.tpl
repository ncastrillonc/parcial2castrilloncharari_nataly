<table border="0" width="100%" cellpadding="0" cellspacing="10">
    <tr>
        <td>
            <h4>Jugadores del Equipo {$codeq}</h4>
        </td>
    </tr>
    {section loop=$jugadores name=i}
    <tr>
        <td>
            <b>Cedula : </b> {$jugadores[i]->get('cedula')}<br />
            <b>Nombre : </b> {$jugadores[i]->get('nombre')}<br />
            <b>Posición : </b> {$jugadores[i]->get('posicion')}<br /><br />
        </td></tr>
    {/section}
</table>
