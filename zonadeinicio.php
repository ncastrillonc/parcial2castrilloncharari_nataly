﻿<?php

require('configs/include.php');

class c_zonadeinicio extends super_controller {
	
	public function display()
	{				
		$this->engine->display('header.tpl');
                
                // http://localhost/Parcial2/zonadeinicio.php?cod=123
                
                if(@$this->get->cod == 123){
                    $this->engine->assign('title','Zona de Inicio');
                    $this->engine->display('zonadeinicio.tpl');
                } else{
                    $this->engine->assign('title','Redireccionando...');
                    $this->engine->display('zonaprohibida.tpl');
                }
		$this->engine->display('footer.tpl');
	}
        
	public function run()
	{
		$this->display();
	}
}

$call = new c_zonadeinicio();
$call->run();

?>