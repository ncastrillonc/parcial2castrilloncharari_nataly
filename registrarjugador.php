<?php

require('configs/include.php');

class c_registrarjugador extends super_controller {

    public function add()
    {
        function existeEquipo($objeto, $codi)
        {
            $objeto->orm->connect();

            $cod['equipo']['codigo'] = $codi;
            $options['equipo']['lvl2']="count_by_cod";
            $objeto->orm->read_data(array("equipo"),$options,$cod);
                                    
            $datos = $objeto->orm->data;
            $resultado = $datos['equipo'][0];
            $contador = $resultado->contador;

            $objeto->orm->close();

            if($contador ==  1)
            {
                return 1;
            } else {
                return 0;
            }
        }
        
        $jugador = new jugador($this->post);
        
        if(is_empty($jugador->get('cedula'))) {
            throw_exception("Debe ingresar una cédula para el Jugador");
        }
        
        if(is_empty($jugador->get('equipo'))) {
            throw_exception("Debe ingresar un código de Equipo para el Jugador");
        }
        
        if(existeEquipo($this, $this->post->equipo) == 0){
            $eq = $this->post->equipo;
            throw_exception("El Equipo con código = $eq no existe");
        }
		
        $this->orm->connect();
        $this->orm->insert_data("normal",$jugador);   
        $this->orm->close();
        
        $this->type_warning = "success";
        $this->msg_warning = "Jugador agregado correctamente";
        
        $this->temp_aux = 'message.tpl';
        $this->engine->assign('type_warning',$this->type_warning);
        $this->engine->assign('msg_warning',$this->msg_warning);
    }

    public function display()
    {
        $this->engine->assign('title','Registrar Jugador');
        $this->engine->display('header.tpl');
        $this->engine->display($this->temp_aux);
        $this->engine->display('registrarjugador.tpl');
        $this->engine->display('footer.tpl');
    }
    
    public function run()
    {
        try {
            if (isset($this->get->option))
            {
                $this->{$this->get->option}();
            }
        }
        catch (Exception $e) 
        {
            $this->error=1; $this->msg_warning=$e->getMessage();
            $this->engine->assign('type_warning',$this->type_warning);
            $this->engine->assign('msg_warning',$this->msg_warning);
            $this->temp_aux = 'message.tpl';
        }    
        $this->display();
    }
}

$call = new c_registrarjugador();
$call->run();

?>
